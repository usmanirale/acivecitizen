<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Credit;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Session;

use Auth;

class CreditsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $credit = Credit::where('user_id', Auth::user()->id)->first();
        $transactions = Transaction::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->take(10)->get();

        return view('credits.index', compact('credit', 'transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('credits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Credit::create($requestData);

        Session::flash('flash_message', 'Credit added!');

        return redirect('credits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $credit = Credit::findOrFail($id);

        return view('credits.show', compact('credit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $credit = Credit::findOrFail($id);

        return view('credits.edit', compact('credit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $credit = Credit::findOrFail($id);
        $credit->update($requestData);

        Session::flash('flash_message', 'Credit updated!');

        return redirect('credits');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Credit::destroy($id);

        Session::flash('flash_message', 'Credit deleted!');

        return redirect('credits');
    }
}
