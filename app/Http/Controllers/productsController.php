<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\product;
use Illuminate\Http\Request;
use Session;
use App\Helpers\Uploader;
use Auth;

class productsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = product::paginate(25);

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->except('product_image_reference');
        $requestData['product_image_reference'] = "";
        $requestData['user_id'] = Auth::user()->id;

        $product = product::create($requestData);

        if($request->hasFile('product_image_reference')){

            $uploader = new Uploader();
            $main_dir = env("UPLOAD_DIR").'/products/';
            $upload_dir = $uploader->getUploadDir($product->id, $main_dir);

            $extension = $request->product_image_reference->extension();
            $filename = uniqid().$product->id.'.'.$extension;
            $new_filename = $upload_dir.'/'. $filename;

            $upload = $uploader->upload($request->product_image_reference, $new_filename, '');

            //update the product
            $updated_product = Product::find($product->id);
            $product->product_image_reference = $filename;
            $product->save();
        }

        return response()->json(['status' => 'done']);

        // Session::flash('flash_message', 'product added!');

        // return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $product = product::findOrFail($id);
        $product->update($requestData);

        Session::flash('flash_message', 'product updated!');

        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        product::destroy($id);

        Session::flash('flash_message', 'product deleted!');

        return redirect('products');
    }
}