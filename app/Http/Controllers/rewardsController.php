<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Reward;
use Illuminate\Http\Request;
use Session;
use App\Helpers\Uploader;
use Auth;

class rewardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $rewards = Reward::paginate(25);

        return view('rewards.index', compact('rewards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('rewards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->except('reward_image_reference');
        $requestData['reward_image_reference'] = "";
        $reward = Reward::create($requestData);

        if($request->hasFile('reward_image_reference')){

            $uploader = new Uploader();
            $main_dir = env("UPLOAD_DIR").'/rewards/';
            $upload_dir = $uploader->getUploadDir($reward->id, $main_dir);

            $extension = $request->reward_image_reference->extension();
            $filename = uniqid().$reward->id.'.'.$extension;
            $new_filename = $upload_dir.'/'. $filename;

            $upload = $uploader->upload($request->reward_image_reference, $new_filename, '');

            //update the reward
            $reward->reward_image_reference = $filename;
            $reward->save();
        }

        return response()->json(['status' => 'done']);

        // Session::flash('flash_message', 'reward added!');

        // return redirect('rewards');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reward = Reward::findOrFail($id);

        return view('rewards.show', compact('reward'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $reward = Reward::findOrFail($id);

        return view('rewards.edit', compact('reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $reward = Reward::findOrFail($id);
        $reward->update($requestData);

        Session::flash('flash_message', 'reward updated!');

        return redirect('rewards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Reward::destroy($id);

        Session::flash('flash_message', 'reward deleted!');

        return redirect('rewards');
    }
}
