<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignAction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campaign_actions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = ['campaign_id', 'action_id', 'message', 'instructions', 'hashtags', 'deleted_at'];

    public function Campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    public function action()
    {
        return $this->belongsTo('App\Models\Action');
    }


}
