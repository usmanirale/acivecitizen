<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('reward_time');
            $table->string('reward_date');
            $table->string('reward_image_reference')->nullable();
            $table->integer('points');
            $table->text('location');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rewards');
    }
}
