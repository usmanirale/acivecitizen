<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCampaignActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_actions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('action_id');
            $table->text('message');
            $table->text('instructions');
            $table->text('hashtags');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_actions');
    }
}
