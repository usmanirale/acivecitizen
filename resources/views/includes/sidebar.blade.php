<!-- BEGIN MAIN SIDEBAR -->
        <nav id="sidebar">
            <div id="main-menu">
                <ul class="sidebar-nav">
                    <li>
                        <a href="/admin"><i class="fa fa-dashboard"></i><span class="sidebar-text">Dashboard</span></a>
                    </li>
                     <li class="active current hasSub">
                        <a href="#"><i class="glyph-icon flaticon-email"></i><span class="sidebar-text">Campaigns</span><span class="fa arrow"></span></a>
                        <ul class="submenu collapse">
                            <li class="current">
                                <a href="/campaigns/create"><span class="sidebar-text">Add Campaigns</span></a>
                            </li>
                            <li>
                                <a href="/campaigns"><span class="sidebar-text">View Campaigns</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="glyph-icon flaticon-email"></i><span class="sidebar-text">Rewards</span><span class="fa arrow"></span></a>
                        <ul class="submenu collapse">
                            <li>
                                <a href="/rewards/create"><span class="sidebar-text">Add Rewards</span></a>
                            </li>
                            <li>
                                <a href="/rewards"><span class="sidebar-text">View Rewards</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="glyph-icon flaticon-forms"></i><span class="sidebar-text">Products</span><span class="fa arrow"></span></a>
                        <ul class="submenu collapse">
                            <li>
                                <a href="/products/create"><span class="sidebar-text">Add Products</span></a>
                            </li>
                            <li>
                                <a href="/products"><span class="sidebar-text">View Products</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="glyph-icon flaticon-ui-elements2"></i><span class="sidebar-text">Meme</span><span class="fa arrow"></span></a>
                        <ul class="submenu collapse">
                            <li>
                                <a href="/memes/create"><span class="sidebar-text">Add Meme</span></a>
                            </li>
                            <li>
                                <a href="/memes"><span class="sidebar-text">View Meme</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/credits"><i class="fa fa-dashboard"></i><span class="sidebar-text">Credits</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- END MAIN SIDEBAR -->