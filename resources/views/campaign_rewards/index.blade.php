@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Campaign_rewards</div>
                    <div class="panel-body">

                        <a href="{{ url('/campaign_rewards/create') }}" class="btn btn-primary btn-xs" title="Add New campaign_reward"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Campaign Id </th><th> Reward Id </th><th> Deleted At </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($campaign_rewards as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->campaign_id }}</td><td>{{ $item->reward_id }}</td><td>{{ $item->deleted_at }}</td>
                                        <td>
                                            <a href="{{ url('/campaign_rewards/' . $item->id) }}" class="btn btn-success btn-xs" title="View campaign_reward"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/campaign_rewards/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit campaign_reward"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/campaign_rewards', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete campaign_reward" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete campaign_reward',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $campaign_rewards->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection