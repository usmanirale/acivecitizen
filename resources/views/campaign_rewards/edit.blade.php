@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit campaign_reward {{ $campaign_reward->id }}</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($campaign_reward, [
                            'method' => 'PATCH',
                            'url' => ['/campaign_rewards', $campaign_reward->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('campaign_rewards.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection