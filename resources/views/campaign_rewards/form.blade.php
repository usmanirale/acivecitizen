<div class="form-group {{ $errors->has('campaign_id') ? 'has-error' : ''}}">
    {!! Form::label('campaign_id', 'Campaign Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('campaign_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('campaign_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reward_id') ? 'has-error' : ''}}">
    {!! Form::label('reward_id', 'Reward Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('reward_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reward_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('deleted_at') ? 'has-error' : ''}}">
    {!! Form::label('deleted_at', 'Deleted At', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'deleted_at', null, ['class' => 'form-control']) !!}
        {!! $errors->first('deleted_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>