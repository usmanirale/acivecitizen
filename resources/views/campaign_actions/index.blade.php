@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Campaign_actions</div>
                    <div class="panel-body">

                        <a href="{{ url('/campaign_actions/create') }}" class="btn btn-primary btn-xs" title="Add New campaign_action"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Campaign Id </th><th> Action Id </th><th> Message </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($campaign_actions as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->campaign_id }}</td><td>{{ $item->action_id }}</td><td>{{ $item->message }}</td>
                                        <td>
                                            <a href="{{ url('/campaign_actions/' . $item->id) }}" class="btn btn-success btn-xs" title="View campaign_action"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/campaign_actions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit campaign_action"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/campaign_actions', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete campaign_action" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete campaign_action',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $campaign_actions->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection