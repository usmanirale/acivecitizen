<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('credits') ? 'has-error' : ''}}">
    {!! Form::label('credits', 'Credits', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('credits', null, ['class' => 'form-control']) !!}
        {!! $errors->first('credits', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('deleted_at') ? 'has-error' : ''}}">
    {!! Form::label('deleted_at', 'Deleted At', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'deleted_at', null, ['class' => 'form-control']) !!}
        {!! $errors->first('deleted_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>