@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Credit {{ $credit->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('credits/' . $credit->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Credit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['credits', $credit->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Credit',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $credit->id }}</td>
                                    </tr>
                                    <tr><th> User Id </th><td> {{ $credit->user_id }} </td></tr><tr><th> Credits </th><td> {{ $credit->credits }} </td></tr><tr><th> Deleted At </th><td> {{ $credit->deleted_at }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection