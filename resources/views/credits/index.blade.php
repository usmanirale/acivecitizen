<!DOCTYPE html>

<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>ACTIVE CITIZEN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/dashboard/assets/img/A.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/dashboard/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/plugins.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/dante-editor.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.circliful.css">
    <link href="/dashboard/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <!-- END  MANDATORY STYLE -->
    <script src="/dashboard/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>


<body data-page="blank_page">
    <!-- BEGIN TOP MENU -->
    @include('includes.menu')
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        <!-- BEGIN MAIN SIDEBAR -->
        @include('includes.sidebar')
        <!-- END MAIN SIDEBAR -->
        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Credits</h1>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    @if(Session::has('flash_message'))
                    <div class="row">
                        <div class="col-md-12">
                           <div class="alert alert-success">{{Session::get('flash_message')}}</div>
                        </div>
                    </div>
                    @endif
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <h3 class="panel-title">Transactions</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-5">Balance (#):</div>
                                <div class="col-md-7 fw-900">@if(isset($credit)){{$credit->credits}} @else 0.00 @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">Customer Name:</div>
                                <div class="col-md-7 fw-900">{{Auth::user()->name}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 fw-900">Recent Transactions</div>
                            </div>
                            <div class="row" id="transactionsList">
                                @if(isset($transactions))
                                    @foreach($transactions as $transaction)
                                    <div class="col-md-12">
                                        <div class="col-md-5">{{$transaction->created_at}}</div>
                                        <div class="col-md-7">Active citizen payment card</div>
                                        <hr>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="row">
                                <br>
                                <br>
                                <div class="col-md-12 text-center">
                                    <a href="#" id="more">more</a>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="text-center">
                        <div class="form-group">
                            <input class="form-control" id="amount" placeholder="Enter amount to credit account"></input>
                            {{csrf_field()}}
                        </div>

                        <a href="#" class="btn btn-custom" onclick="payWithPaystack()">Buy Credit</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/plugins/jquery-1.11.js"></script>
    <script src="/dashboard/assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/dashboard/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="/dashboard/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/dashboard/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="/dashboard/assets/plugins/nprogress/nprogress.js"></script>
    {{-- <script src="/dashboard/assets/plugins/charts-sparkline/sparkline.min.js"></script> --}}
    <script src="/dashboard/assets/plugins/breakpoints/breakpoints.js"></script>
    <script src="/dashboard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!--  Charts Plugins -->
    <!-- END MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/js/application.js"></script>
    <script src="/dashboard/assets/js/main.js"></script>
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

           app.searchToggler();
           // demo.wizard();
           $("#more").on("click", function(){
                //show more or less depending on the text
                $(this).text() === 'more' ? $(this).text('less') : $(this).text('more');
                //fadeToggle
                $("#transactionsList > :nth-child(n + 3)").fadeToggle();
           });
        });

        var tranRef = "<?php echo md5(Auth::user()->id).uniqid(5);?>";
        function payWithPaystack(){
            var handler = PaystackPop.setup({
                key: "<?php echo env("PAYSTACK_KEY");?>",
                email: "{{Auth::user()->email}}",
                amount: parseInt($("#amount").val()) * 100,
                ref: tranRef,
                callback: function(response){

                data = {"reference": response.reference, "amount": parseInt($("#amount").val()), "payment_status": "success", "_token": $('input[name=_token]').val()};
                $.post('/transactions', data, function(){
                    event.preventDefault();
                    window.location.href = "/credits";
                });
                },
                onClose: function(){
                  console.log('window closed');
                }
            });
            handler.openIframe();
        }
    </script>
</body>
</html>
