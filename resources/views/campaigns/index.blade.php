<!DOCTYPE html>

<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>ACTIVE CITIZEN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/dashboard/assets/img/A.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/dashboard/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/plugins.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/dante-editor.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.circliful.css">
    <link href="/dashboard/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/dashboard/assets/css/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/main.css">

    <!-- END  MANDATORY STYLE -->
    <script src="/dashboard/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>


<body data-page="view_campaigns">
    <!-- BEGIN TOP MENU -->
    @include('includes.menu')
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        <!-- BEGIN MAIN SIDEBAR -->
        @include('includes.sidebar')
        <!-- END MAIN SIDEBAR -->
        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Campaigns</h1>
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page-notes">
                                    <div class="content clearfix">
                                        <div id="campaigns">
                                            @if(!isset($campaigns))
                                            <div class="no-data text-center" id="no-data">
                                                <div class="m-b-md">
                                                    <span class="sprite sprite-list" style="background-image: url(/dashboard/assets/img/add.png);"></span>
                                                </div>
                                                 No campaigns yet...
                                            </div>
                                           @else

                                            <div id="yes-data" >
                                                <div class="top-menu" >
                                                    <a class="btn btn-dark" href='#'><strong>All</strong> <span class="label label-danger m-l-10" id="total_users">{{$total_campaigns}}</span></a>
                                                    <a href="#" class="btn btn-default">Active <span class="label label-default m-l-10">{{$active_campaigns}}</span></a>
                                                    <a href="#" class="btn btn-default">Pending<span class="label label-default m-l-10">{{$pending_campaigns}}</span></a>
                                                    <a href="#" class="btn btn-default">Deleted <span class="label label-default m-l-10" id="deleted_user">{{$deleted_campaigns}}</span></a>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="events-filter clearfix">
                                                            <div class="col-md-3">
                                                                <div id="reportrange" class="form-control m-b-10">
                                                                    <i class="fa fa-calendar fa-lg p-r-10"></i> <span>Select a date...</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select data-width="100%" class="selectpicker" title="Select a category...">
                                                                    <option class="no-option">&nbsp;</option>
                                                                    <option value="music">Music</option>
                                                                    <option value="art">Art</option>
                                                                    <option value="cinema">Cinema</option>
                                                                    <option value="culture">Culture</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="" class="selectpicker" data-width="100%" title="Select a location...">
                                                                    <option class="no-option">&nbsp;</option>
                                                                    <option value="ny">New York</option>
                                                                    <option value="chicago">Chicago</option>
                                                                    <option value="s-f">San Francisco</option>
                                                                    <option value="miami">Miami</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" placeholder="Search artist, keyword...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 table-responsive">
                                                        <table id="events-table" class="table table-tools table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th style="max-width:200px">Cover</th>
                                                                    <th>Campaign</th>
                                                                    <th>Categories</th>
                                                                    <th style="width:15%">Rewards</th>
                                                                    <th class="text-center">Date</th>

                                                                    <th class="text-center">Status</th>
                                                                    <th class="text-center">Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                @foreach($campaigns as $campaign)
                                                                <tr>
                                                                        <td>
                                                                            <?php
                                                                                $upload_folder = md5($campaign->id);
                                                                                $image_url = getenv("APP_URL").'/upload/campaigns/'.$upload_folder.'/'.$campaign->image_reference;
                                                                            ?>
                                                                            <a href="{{ url('/campaigns/' . $campaign->id . '/edit') }}">
                                                                                <img src="{{$image_url}}" alt="mars" class="img-responsive" />
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" class="c-red"> {{$campaign->title}} </a>
                                                                        </td>
                                                                        <td>Social</td>
                                                                        <td>
                                                                            <a class="button" data-toggle="modal" href="#modal-id">Add</a>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <div class="event-date">
                                                                                <div class="event-month">
                                                                                    <span class="dots">&nbsp;</span>
                                                                                        <span title="{{date('M', strtotime($campaign->created_at))}}"> {{date('M', strtotime($campaign->created_at))}}</span>
                                                                                        <span class="dots">&nbsp;</span>
                                                                                </div>
                                                                                <div class="event-day">{{date('jS', strtotime($campaign->created_at))}}</div>
                                                                                <div class="event-day-txt">{{date('D', strtotime($campaign->created_at))}}</div>
                                                                            </div>
                                                                        </td>

                                                                        <td class="text-center">
                                                                            <span class="label label-success w-300">{{$campaign->status}}</span>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a href="{{ url('/campaigns/' . $campaign->id . '/edit') }}" data-rel="tooltip" title="Edit this Event" class="edit btn btn-sm btn-icon btn-rounded btn-default">
                                                                                <i class="fa fa-pencil"></i>
                                                                            </a>


                                                                        {!! Form::open([
                                                                            'method'=>'DELETE',
                                                                            'url' => ['/campaigns', $campaign->id],
                                                                            'style' => 'display:inline'
                                                                        ]) !!}
                                                                            {!! Form::button('<i class="fa fa-times"></i>', array(
                                                                                    'type' => 'submit',
                                                                                    'data-rel' => 'tooltip',
                                                                                    'class' => 'delete btn btn-sm btn-icon btn-rounded btn-default',
                                                                                    'title' => 'Delete Campaign',
                                                                                    'onclick'=>'return confirm("Confirm delete?")'
                                                                            )) !!}
                                                                        {!! Form::close() !!}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!--BS MODAL HERE-->
    <div class="modal" id="modal-id">
        <div class="modal-dialog animated flipInY">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Reward</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--BS MODAL ENDS HERE-->


    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/plugins/jquery-1.11.js"></script>
    <script src="/dashboard/assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/dashboard/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="/dashboard/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/dashboard/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="/dashboard/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/dashboard/assets/plugins/breakpoints/breakpoints.js"></script>
    <script src="/dashboard/assets/plugins/numerator/jquery-numerator.js"></script>
    <script src="/dashboard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!--  Charts Plugins -->
    <script src="/dashboard/assets/plugins/underscore-min.js"></script>
    <script src="/dashboard/assets/plugins/sanitize.js"></script>
    <script src="/dashboard/assets/plugins/dante-editor.js"></script>
    <script src="/dashboard/assets/plugins/jquery.dataTables.min.js"></script>
    <script src="/dashboard/assets/plugins/dataTables.bootstrap.js"></script>
    <script src="/dashboard/assets/plugins/dataTables.tableTools.js"></script>
    <script src="/dashboard/assets/plugins/moment.min.js"></script>
    <script src="/dashboard/assets/plugins/daterangepicker.js"></script>


    <!--<script src="/dashboard/assets/plugins/jquery.bootstrap.wizard.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select.js"></script>-->
    <!-- END MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/js/application.js"></script>
    <script src="/dashboard/assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            // app.selectPicker();
            // app.toggleIconsHandler();
            // app.campaignsHandler();
            // app.dateRanger();
            // app.searchToggler();

        });
    </script>
</body>
</html>
