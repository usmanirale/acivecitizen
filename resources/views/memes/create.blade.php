<!DOCTYPE html>

<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>ACTIVE CITIZEN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/dashboard/assets/img/A.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/dashboard/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/plugins.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/dante-editor.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.circliful.css">
    <link href="/dashboard/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/spectrum.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.drag-n-crop.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/main.css">
    <link rel="stylesheet" href="/dashboard/assets/css/simple-line-icons/css/simple-line-icons.css">
    <!-- END  MANDATORY STYLE -->
    <script src="/dashboard/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>


<body data-page="blank_page">
    <!-- BEGIN TOP MENU -->
    @include('includes.menu')
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        <!-- BEGIN MAIN SIDEBAR -->
        @include('includes.sidebar')
        <!-- END MAIN SIDEBAR -->
        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Memes</h1>
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page-notes">
                                    <div class="content clearfix">
                                        <div id="memes">
                                            <div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="events-filter clearfix">

                                                            <div class="col-md-3" style="display: none;">
                                                                <input type="file" class="form-control" placeholder="Search artist, keyword...">
                                                            </div>

                                                            <div class="col-md-3">
                                                                <input type="text" class="form-control" placeholder="Customize your meme..." name="texts">
                                                            </div>

                                                        </div>
                                                        <div class="events-filter clearfix">
                                                            <div class="col-md-12">
                                                                <div class="btn-group">
                                                                  <button type="button" class="btn btn-default" name="bold">
                                                                    <i class="fa fa-bold"></i>
                                                                  </button>
                                                                  <button type="button" class="btn btn-default" name="italics">
                                                                    <i class="fa fa-italic"></i>
                                                                  </button>
                                                                  <button type="button" class="btn btn-default" name="strikethrough">
                                                                      <i class="fa fa-strikethrough"></i>
                                                                  </button>
                                                                  <button type="button" class="btn btn-default" name="underline">
                                                                      <i class="fa fa-underline"></i>
                                                                  </button>
                                                                </div>
                                                                <div class="btn-group">
                                                                  <button type="button" class="btn btn-default" name="left"><i class="fa fa-align-left"></i></button>
                                                                  <button type="button" class="btn btn-default" name="center"><i class="fa fa-align-center"></i></button>
                                                                  <button type="button" class="btn btn-default" name="right"><i class="fa fa-align-right"></i></button>
                                                                  <button type="button" class="btn btn-default" name="top"><i class="fa fa-chevron-up"></i></button>
                                                                  <button type="button" class="btn btn-default" name="middle"><i class="fa fa-bars"></i></button>
                                                                  <button type="button" class="btn btn-default" name="bottom"><i class="fa fa-chevron-down"></i></button>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <input type="text" class="form-control" placeholder="background..." name="background">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default" id="upload">Upload</button>
                                                                </div>
                                                                <div class="btn-group">
                                                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="font_style">
                                                                    Style <span class="caret"></span>
                                                                  </button>
                                                                  <ul class="dropdown-menu fonts">
                                                                    <li><a href="#">Times New Roman</a></li>
                                                                    <li><a href="#">SegoeUI</a></li>
                                                                    <li><a href="#">Avenir</a></li>
                                                                    <li><a href="#">Helvetica</a></li>
                                                                    <li><a href="#">Arial Black</a></li>
                                                                    <li><a href="#">Century</a></li>
                                                                  </ul>
                                                                </div>
                                                                <div class="btn-group">
                                                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="font_size">
                                                                    Size <span class="caret"></span>
                                                                  </button>
                                                                  <ul class="dropdown-menu sizes">
                                                                    <li><a href="#">8</a></li>
                                                                    <li><a href="#">9</a></li>
                                                                    <li><a href="#">10</a></li>
                                                                    <li><a href="#">11</a></li>
                                                                    <li><a href="#">12</a></li>
                                                                    <li><a href="#">13</a></li>
                                                                    <li><a href="#">14</a></li>
                                                                    <li><a href="#">15</a></li>
                                                                    <li><a href="#">16</a></li>
                                                                    <li><a href="#">17</a></li>
                                                                    <li><a href="#">18</a></li>
                                                                    <li><a href="#">19</a></li>
                                                                    <li><a href="#">20</a></li>
                                                                    <li><a href="#">22</a></li>
                                                                    <li><a href="#">24</a></li>
                                                                    <li><a href="#">26</a></li>
                                                                    <li><a href="#">28</a></li>
                                                                    <li><a href="#">36</a></li>
                                                                    <li><a href="#">48</a></li>
                                                                    <li><a href="#">72</a></li>
                                                                  </ul>
                                                                </div>
                                                                <div class="btn-group">
                                                                  <button type="button" class="btn btn-default"><i class="fa fa-bell"></i></button>
                                                                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <input type="text" class="form-control" placeholder="font color..." name="font_color" value="font-color">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-t-40">
                                                    <div class="col-md-6">
                                                        <div class="card clearfix">
                                                            <div class="header">
                                                                <h4 class="title">Image Meme</h4>
                                                            </div>
                                                            <div class="content clearfix">
                                                                <div class="meme" id="imageContainer">
                                                                    <textarea></textarea>
                                                                    <img src="/dashboard/assets/img/rewards/jumia.jfif" id="memePhoto">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="card clearfix">
                                                            <div class="header">
                                                                <h4 class="title">Preview</h4>
                                                            </div>
                                                            <div class="content clearfix">
                                                                <div class="meme" id="previewContainer">
                                                                    <textarea></textarea>
                                                                    <img src="/dashboard/assets/img/rewards/jumia.jfif" id="preview">
                                                                </div>
                                                                <br>
                                                                <br>

                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn btn-custom" id="saveMeme">Publish</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <form name="postImage" id="postImage">
                                                <input type="hidden" name="image_reference" id="hidden_image">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <script src="/dashboard/assets/plugins/jquery-1.11.js"></script>
    <script src="/dashboard/assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/dashboard/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="/dashboard/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/dashboard/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="/dashboard/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/dashboard/assets/plugins/charts-sparkline/sparkline.min.js"></script>
    <script src="/dashboard/assets/plugins/breakpoints/breakpoints.js"></script>
    <script src="/dashboard/assets/plugins/numerator/jquery-numerator.js"></script>
    <script src="/dashboard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!--  Charts Plugins -->
    <script src="/dashboard/assets/plugins/chartist.min.js"></script>
    <script src="/dashboard/assets/plugins/Chart.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.circliful.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.animator.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.resize.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.time.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-notify.js"></script>
    <script src="/dashboard/assets/plugins/waypoint.js"></script>
    <script src="/dashboard/assets/plugins/countUp.js"></script>
    <script src="/dashboard/assets/plugins/spectrum.js"></script>
    <script src="/dashboard/assets/plugins/imagesLoaded.js"></script>
    <script src="/dashboard/assets/plugins/jquery.drag-n-crop.js"></script>
    <script src="/dashboard/assets/plugins/html2canvas.js"></script>
    <script src="/dashboard/assets/plugins/base64.min.js"></script>
    <script src="/dashboard/assets/plugins/canvas2image.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/js/application.js"></script>
    <script src="/dashboard/assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            //app.memeHandler();
           // demo.wizard();
           app.memeHandler2();
           app.searchToggler();
            //console.log(type);

        });
    </script>
</body>
</html>
