@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">meme {{ $meme->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('memes/' . $meme->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit meme"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['memes', $meme->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete meme',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $meme->id }}</td>
                                    </tr>
                                    <tr><th> Image Reference </th><td> {{ $meme->image_reference }} </td></tr><tr><th> User Id </th><td> {{ $meme->user_id }} </td></tr><tr><th> Deleted At </th><td> {{ $meme->deleted_at }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection