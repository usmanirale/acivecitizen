<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('actions', 'actionsController');
Route::resource('campaign_actions', 'campaign_actionsController');
Route::resource('campaign_rewards', 'campaign_rewardsController');
Route::resource('campaigns', 'campaignsController');
Route::resource('memes', 'memesController');
Route::resource('products', 'productsController');
Route::resource('rewards', 'rewardsController');
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('user_types', 'user_typesController');
Route::resource('transactions', 'TransactionsController');
Route::resource('credits', 'CreditsController');