# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'active-citizen.com'
set :repo_url, 'git@bitbucket.org:usmanirale/acivecitizen.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, "master"
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/html/staging/active-citizen'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true
set :use_sudo, false
